const setLanguage = (lg) => {
    switch (lg) {
        case "en":
            window.language = require("./en").default
            break

        default:
            window.language = require("./en").default
            break
    }
}

const getLanguage = () => {
    if (!window.language) {
        window.language = require("./en").default
    }
    return window.language
}

export { getLanguage, setLanguage }
