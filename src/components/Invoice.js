import React, { Component } from "react"
import Image from "./image"
import { getLanguage } from "../languajes"
const Invoice = ({ invoice }) => {
    const {
        id,
        items,
        total,
        subtotal,
        customer,
        facility,
        payments,
        date,
        due,
    } = invoice
    return (
        <div className="invoice-box">
            <table cellPadding="0" cellSpacing="0">
                <tbody>
                    <tr className="top">
                        <td colSpan="4">
                            <table>
                                <tbody>
                                    <tr>
                                        <td className="title">
                                            <Image
                                                src={facility.image}
                                                style={{
                                                    width: "100%",
                                                    maxWidth: "300px",
                                                }}
                                            />
                                        </td>

                                        <td>
                                            Invoice #: {id}
                                            <br />
                                            Created: {date}
                                            <br />
                                            Due: {due}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr className="information">
                        <td colSpan="4">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            {facility.name}
                                            <br />
                                            {facility.description}
                                            <br />
                                            {facility.code}
                                        </td>

                                        <td>
                                            {customer.name}
                                            <br />
                                            {customer.dni}
                                            <br />
                                            {customer.email}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr className="heading">
                        <td colSpan="3">{getLanguage().PaymentsMethodLabel}</td>

                        <td>{getLanguage().AmountLabel}</td>
                    </tr>
                    {payments ? (
                        payments.map((p) => (
                            <tr className="details">
                                <td colSpan="3">{p.name}</td>
                                <td>{p.amount}</td>
                            </tr>
                        ))
                    ) : (
                        <tr className="details">
                            <td colSpan="4">{getLanguage().NoPayments}</td>
                        </tr>
                    )}

                    <tr className="heading">
                        <td>{getLanguage().QuantityLabel}</td>
                        <td>{getLanguage().ItemLabel}</td>
                        <td>{getLanguage().PriceLabel}</td>
                        <td>{getLanguage().TotalLabel}</td>
                    </tr>

                    {items.map((i, k, a) => (
                        <tr
                            key={i.id}
                            className={
                                "item " + (k == a.length - 1 ? "last" : "")
                            }
                        >
                            <td>{i.quantity}</td>

                            <td style={{ textAlign: "left" }}>{i.name}</td>

                            <td>${i.price.toFixed(2)}</td>
                            <td>${i.total.toFixed(2)}</td>
                        </tr>
                    ))}

                    <tr className="total">
                        <td colSpan="2"></td>
                        <td>{getLanguage().TotalLabel}:</td>
                        <td>{total.toFixed(2)}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default Invoice
