import React, { Component } from "react"
import CartScrollBar from "./CartScrollBar"
import CartItem from "./CartItem"
import EmptyCart from "../empty-states/EmptyCart"
import CSSTransitionGroup from "react-transition-group/CSSTransitionGroup"
import { findDOMNode } from "react-dom"
import { getUser } from "../scripts/user"
import Icon from "./Icon"
import { getLanguage } from "../languajes"

class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showCart: false,
            cart: this.props.cartItems,
            mobileSearch: false,
        }
    }
    handleCart(e) {
        e.preventDefault()
        this.setState({
            showCart: !this.state.showCart,
        })
    }
    handleSubmit(e) {
        e.preventDefault()
    }
    handleMobileSearch(e) {
        e.preventDefault()
        this.setState({
            mobileSearch: true,
        })
    }
    handleSearchNav(e) {
        e.preventDefault()
        this.setState(
            {
                mobileSearch: false,
            },
            function () {
                this.refs.searchBox.value = ""
                this.props.handleMobileSearch()
            }
        )
    }
    handleClickOutside(event) {
        const cartNode = findDOMNode(this.refs.cartPreview)
        if (cartNode.classList.contains("active")) {
            if (!cartNode || !cartNode.contains(event.target)) {
                this.setState({
                    showCart: false,
                })
                event.stopPropagation()
            }
        }
    }
    componentDidMount() {
        document.addEventListener(
            "click",
            this.handleClickOutside.bind(this),
            true
        )
    }
    componentWillUnmount() {
        document.removeEventListener(
            "click",
            this.handleClickOutside.bind(this),
            true
        )
    }
    render() {
        const user = getUser()
        let cartItems
        const { facility } = this.props
        const {
            name,
            image,
            description,
            searchPlaceholder,
            itemsCountLabel,
            checkoutLabel,
            loginLabel,
            emptyCartImage,
            emptyCartText,
        } = facility || {}
        cartItems = this.state.cart.map((product) => (
            <CartItem
                key={product.id}
                product={product}
                onRemove={(pid) => this.props.removeProduct(pid)}
            />
        ))
        let view
        if (cartItems.length <= 0) {
            view = <EmptyCart />
        } else {
            view = (
                <CSSTransitionGroup
                    transitionName="fadeIn"
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={300}
                    component="ul"
                    className="cart-items"
                >
                    {cartItems}
                </CSSTransitionGroup>
            )
        }
        return (
            <header>
                <div className="container">
                    <div className="brand">
                        <img
                            className="logo"
                            src={image || require("../images/logo.png")}
                            style={{ width: 204, height: 60 }}
                            alt="eapi logo"
                        />
                    </div>

                    <div className="search">
                        <Icon onClick={this.handleMobileSearch.bind(this)}>
                            <svg
                                className="mobile-search"
                                id="searchicon"
                                height="512"
                                viewBox="0 0 512.002 512.002"
                                width="512"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="#174278"
                            >
                                <g>
                                    <path d="m495.594 416.408-134.086-134.095c14.685-27.49 22.492-58.333 22.492-90.312 0-50.518-19.461-98.217-54.8-134.31-35.283-36.036-82.45-56.505-132.808-57.636-1.46-.033-2.92-.054-4.392-.054-105.869 0-192 86.131-192 192s86.131 192 192 192c1.459 0 2.93-.021 4.377-.054 30.456-.68 59.739-8.444 85.936-22.436l134.085 134.075c10.57 10.584 24.634 16.414 39.601 16.414s29.031-5.83 39.589-16.403c10.584-10.577 16.413-24.639 16.413-39.597s-5.827-29.019-16.407-39.592zm-299.932-64.453c-1.211.027-2.441.046-3.662.046-88.224 0-160-71.776-160-160s71.776-160 160-160c1.229 0 2.449.019 3.671.046 86.2 1.935 156.329 73.69 156.329 159.954 0 86.274-70.133 158.029-156.338 159.954z" />
                                    <path d="m192 320.001c-70.58 0-128-57.42-128-128s57.42-128 128-128 128 57.42 128 128-57.42 128-128 128z" />
                                </g>
                            </svg>
                        </Icon>
                        <form
                            action="#"
                            method="get"
                            className={
                                this.state.mobileSearch
                                    ? "search-form active"
                                    : "search-form"
                            }
                        >
                            <Icon onClick={this.handleSearchNav.bind(this)}>
                                <svg
                                    className="back-button"
                                    version="1.1"
                                    id="Capa_1"
                                    xmlns="http://www.w3.org/2000/svg"
                                    x="0px"
                                    y="0px"
                                    width="32px"
                                    height="32px"
                                    viewBox="0 0 299.021 299.021"
                                    fill="#174278"
                                >
                                    <g>
                                        <g>
                                            <path
                                                d="M292.866,254.432c-2.288,0-4.443-1.285-5.5-3.399c-0.354-0.684-28.541-52.949-146.169-54.727v51.977
			c0,2.342-1.333,4.48-3.432,5.513c-2.096,1.033-4.594,0.793-6.461-0.63L2.417,154.392C0.898,153.227,0,151.425,0,149.516
			c0-1.919,0.898-3.72,2.417-4.888l128.893-98.77c1.87-1.426,4.365-1.667,6.461-0.639c2.099,1.026,3.432,3.173,3.432,5.509v54.776
			c3.111-0.198,7.164-0.37,11.947-0.37c43.861,0,145.871,13.952,145.871,143.136c0,2.858-1.964,5.344-4.75,5.993
			C293.802,254.384,293.34,254.432,292.866,254.432z"
                                            />
                                        </g>
                                    </g>
                                </svg>
                            </Icon>

                            <input
                                type="search"
                                ref="searchBox"
                                placeholder={getLanguage().SearchPlaceholder}
                                className="search-keyword"
                                onChange={this.props.handleSearch}
                            />

                            <button
                                className="search-button"
                                type="submit"
                                onClick={this.handleSubmit.bind(this)}
                            />
                        </form>
                    </div>

                    <div className="cart">
                        <Icon
                            name={"$" + this.props.total}
                            count={this.props.totalItems}
                            onClick={this.handleCart.bind(this)}
                        >
                            <svg
                                className={this.props.cartBounce ? "tada" : " "}
                                version="1.1"
                                id="Capa_1"
                                xmlns="http://www.w3.org/2000/svg"
                                x="0px"
                                y="0px"
                                width="32px"
                                height="32px"
                                viewBox="0 0 510 510"
                                fill="#174278"
                            >
                                <g>
                                    <g id="shopping-cart">
                                        <path
                                            d="M153,408c-28.05,0-51,22.95-51,51s22.95,51,51,51s51-22.95,51-51S181.05,408,153,408z M0,0v51h51l91.8,193.8L107.1,306
			c-2.55,7.65-5.1,17.85-5.1,25.5c0,28.05,22.95,51,51,51h306v-51H163.2c-2.55,0-5.1-2.55-5.1-5.1v-2.551l22.95-43.35h188.7
			c20.4,0,35.7-10.2,43.35-25.5L504.9,89.25c5.1-5.1,5.1-7.65,5.1-12.75c0-15.3-10.2-25.5-25.5-25.5H107.1L84.15,0H0z M408,408
			c-28.05,0-51,22.95-51,51s22.95,51,51,51s51-22.95,51-51S436.05,408,408,408z"
                                        />
                                    </g>
                                </g>
                            </svg>
                        </Icon>

                        <div
                            className={
                                this.state.showCart
                                    ? "cart-preview active"
                                    : "cart-preview"
                            }
                            ref="cartPreview"
                        >
                            <CartScrollBar>{view}</CartScrollBar>
                            <div className="action-block">
                                <button
                                    type="button"
                                    className={
                                        this.state.cart.length > 0
                                            ? " "
                                            : "disabled"
                                    }
                                    onClick={() => {
                                        const { cart } = this.state
                                        if (cart.length == 0) return
                                        this.props
                                            .generateInvoice(cart)
                                            .then(() => {
                                                let ids = cart.map((i) => i.id)
                                                ids.forEach((element) =>
                                                    this.props.removeProduct(
                                                        element
                                                    )
                                                )
                                            })
                                    }}
                                >
                                    {getLanguage().CheckOutLabel}
                                </button>
                            </div>
                        </div>
                    </div>
                    <Icon
                        name={user ? user.name : getLanguage().SignInLabel}
                        onClick={() => this.props.onLogout()}
                    >
                        <svg
                            version="1.1"
                            id="usericon"
                            xmlns="http://www.w3.org/2000/svg"
                            x="0px"
                            y="0px"
                            width="32px"
                            height="32px"
                            viewBox="0 0 510 510"
                            fill="#174278"
                        >
                            <g>
                                <g>
                                    <path d="M256,0c-74.439,0-135,60.561-135,135s60.561,135,135,135s135-60.561,135-135S330.439,0,256,0z" />
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path
                                        d="M423.966,358.195C387.006,320.667,338.009,300,286,300h-60c-52.008,0-101.006,20.667-137.966,58.195
			C51.255,395.539,31,444.833,31,497c0,8.284,6.716,15,15,15h420c8.284,0,15-6.716,15-15
			C481,444.833,460.745,395.539,423.966,358.195z"
                                    />
                                </g>
                            </g>
                        </svg>
                    </Icon>
                </div>
            </header>
        )
    }
}

export default Header
