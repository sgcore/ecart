import React, { Component } from "react"
import Product from "./Product"
import LoadingProducts from "../loaders/Products"
import NoResults from "../empty-states/NoResults"
import CSSTransitionGroup from "react-transition-group/CSSTransitionGroup"
import { getLanguage } from "../languajes"
class Products extends Component {
    render() {
        let productsData
        let term = this.props.searchTerm
        let x

        function searchingFor(term) {
            return function (x) {
                return x.name.toLowerCase().includes(term.toLowerCase()) || !term
            }
        }
        productsData = this.props.productsList
            .filter(searchingFor(term))
            .slice(0, 20)
            .map((product) => {
                return (
                    <Product
                        key={product.id}
                        product={product}
                        addToCart={this.props.addToCart}
                        openModal={this.props.openModal}
                        addText={this.props.addText || "add"}
                        addedText={this.props.addedText || "added"}
                    />
                )
            })

        // Empty and Loading States
        let view
        if (productsData.length <= 0 && !term) {
            view = <LoadingProducts />
        } else if (productsData.length <= 0 && term) {
            view = <NoResults />
        } else {
            view = (
                <CSSTransitionGroup
                    transitionName="fadeIn"
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={300}
                    component="div"
                    className="products"
                >
                    {productsData}
                </CSSTransitionGroup>
            )
        }
        return <div className="products-wrapper">{view}</div>
    }
}

export default Products
