import React, { Component } from "react"
import { login, signup } from "../scripts/calls"
import { setUser, getUser } from "../scripts/user"
import Modal from "./Modal"
import Invoices from "./Invoices"
import { getLanguage } from "../languajes"
export default class extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isSignUp: false,
            loading: false,
            error: "",
        }
        this.validatePassword = this.validatePassword.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    componentDidMount() {
        document.body.classList.add("modalbody")
    }

    componentWillUnmount() {
        document.body.classList.remove("modalbody")
    }

    handleSubmit(event) {
        event.preventDefault()
        const data = new FormData(event.target)
        const form = {
            email: data.get("email"),
            password: data.get("password"),
            confirm: data.get("confirm"),
            dni: data.get("dni"),
            name: data.get("name"),
        }
        if (!this.state.isSignUp) {
            login(this, form.email, form.password).then((user) => {
                user && this.props.onLogin()
            })
        } else {
            signup(this, form.email, form.password, form.name, form.dni)
        }
    }
    validatePassword() {
        const password = this.refs.password
        const confirm_password = this.refs.confirm
        if (password.value != confirm_password.value) {
            confirm_password.setCustomValidity(
                getLanguage().PasswordDoesNotMatch
            )
        } else {
            confirm_password.setCustomValidity("")
        }
    }
    render() {
        const { isSignUp, loading, error } = this.state
        const { image } = this.props.config || {}
        const user = getUser()
        if (user) {
            return (
                <Modal onCancel={() => this.props.onCancel()}>
                    <span>
                        <h3>
                            {user.name}{" "}
                            <button
                                className="minibutton"
                                type="button"
                                onClick={() => {
                                    setUser(null)
                                    this.props.onCancel()
                                }}
                            >
                                {getLanguage().LogOutLabel}
                            </button>
                        </h3>
                        <Invoices
                            invoices={user.invoices || []}
                            onSelect={(i) => this.props.onSelectInvoice(i)}
                        ></Invoices>
                    </span>
                </Modal>
            )
        }
        return (
            <Modal onCancel={() => this.props.onCancel()}>
                <div className="register">
                    <h3>
                        {isSignUp
                            ? getLanguage().SignUpLabel
                            : getLanguage().SignInLabel}
                    </h3>
                    <form
                        action=""
                        method="post"
                        className="form"
                        onSubmit={this.handleSubmit}
                    >
                        <div className="form__field">
                            <input
                                id="email"
                                type="email"
                                name="email"
                                placeholder={getLanguage().EmailLabel}
                            />
                        </div>

                        <div className="form__field">
                            <input
                                ref={"password"}
                                type="password"
                                id="password"
                                name="password"
                                placeholder={getLanguage().PasswordLabel}
                                minLength="5"
                            />
                        </div>
                        {isSignUp ? (
                            <div className="form__field">
                                <input
                                    onChange={this.validatePassword}
                                    onKeyUp={this.validatePassword}
                                    ref={"confirm"}
                                    type="password"
                                    id="confirm"
                                    name="confirm"
                                    placeholder={
                                        getLanguage().ConfirmPasswordLabel
                                    }
                                    minLength="5"
                                />
                            </div>
                        ) : null}
                        {isSignUp ? (
                            <div className="form__field">
                                <input
                                    type="text"
                                    id="name"
                                    name="name"
                                    placeholder={
                                        getLanguage().LastAndFirsNameLabel
                                    }
                                    minLength="3"
                                />
                            </div>
                        ) : null}
                        {isSignUp ? (
                            <div className="form__field">
                                <input
                                    type="number"
                                    id="dni"
                                    name="dni"
                                    placeholder={getLanguage().UserCodeLabel}
                                />
                            </div>
                        ) : null}
                        {error ? <p style={{ color: "red" }}>{error}</p> : null}
                        {loading ? null : (
                            <div className="form__field">
                                <input
                                    type="submit"
                                    value={
                                        isSignUp
                                            ? getLanguage().CreateAccountLabel
                                            : getLanguage().SignInLabel
                                    }
                                />
                            </div>
                        )}
                    </form>
                    {loading ? (
                        <div>
                            <h2 style={{ textAlign: "center" }}>{loading}</h2>
                            <div className="lds-hourglass"></div>
                        </div>
                    ) : (
                        <p>
                            {isSignUp ? (
                                <a
                                    href="#"
                                    onClick={() =>
                                        this.setState({
                                            isSignUp: false,
                                            error: "",
                                        })
                                    }
                                >
                                    Cancel
                                </a>
                            ) : (
                                <a
                                    href="#"
                                    onClick={() =>
                                        this.setState({
                                            isSignUp: true,
                                            error: "",
                                        })
                                    }
                                >
                                    {getLanguage().CreateAccountLabel}
                                </a>
                            )}
                        </p>
                    )}
                </div>
            </Modal>
        )
    }
}
