import React, { Component } from "react"
const Image = ({ src, alt, onClick, className, style, badImage }) => (
    <img
        className={className}
        style={style}
        src={src || require(`../images/${badImage || "noImageProduct.png"}`)}
        alt={alt}
        onError={(e) => {
            e.target.onerror = null
            e.target.src = require(`../images/${
                badImage || "noImageProduct.png"
            }`)
        }}
        onClick={onClick}
    />
)
export default Image
