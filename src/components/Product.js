import React, { Component } from "react"
import Counter from "./Counter"
import Image from "./image"

class Product extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedProduct: {},
            quickViewProduct: {},
            isAdded: false,
            quantity: 1,
        }
    }
    addToCart(product, quantity) {
        if (this.state.isAdded) return
        if (!quantity) {
            this.props.addToCart(product)
            return
        }
        this.setState(
            {
                selectedProduct: { quantity, ...product },
            },
            function () {
                this.props.addToCart(this.state.selectedProduct)
            }
        )
        this.setState(
            {
                isAdded: true,
            },
            function () {
                setTimeout(() => {
                    this.setState({
                        isAdded: false,
                        selectedProduct: {},
                    })
                }, 1500)
            }
        )
    }
    quickView(image, name, price, id) {
        this.setState(
            {
                quickViewProduct: {
                    image: image,
                    name: name,
                    price: price,
                    id: id,
                },
            },
            function () {
                this.props.openModal({
                    image: image,
                    name: name,
                    price: price,
                    id: id,
                })
            }
        )
    }
    render() {
        const { product, addText, addedText, hideQuantity } = this.props
        const { image, name, price, id, code, iva } = product || {}
        const { quantity } = this.state
        return (
            <div className="product">
                <div className="product-image">
                    <Image
                        src={image}
                        alt={name}
                        onClick={this.quickView.bind(
                            this,
                            image,
                            name,
                            price,
                            id,
                            quantity
                        )}
                    />
                </div>
                <h4 className="product-name">{name}</h4>
                {price ? (
                    <p className="product-price">
                        {parseFloat(price).toFixed(2)}
                    </p>
                ) : null}
                {!hideQuantity ? (
                    <Counter
                        productQuantity={quantity}
                        updateQuantity={(qty) =>
                            this.setState({ quantity: qty })
                        }
                        resetQuantity={this.resetQuantity}
                    />
                ) : null}
                <div className="product-action">
                    <button
                        className={!this.state.isAdded ? "" : "added"}
                        type="button"
                        onClick={this.addToCart.bind(this, product, quantity)}
                    >
                        {!this.state.isAdded ? addText : addedText}
                    </button>
                </div>
            </div>
        )
    }
}

export default Product
