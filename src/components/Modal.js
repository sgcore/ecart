import React, { Component } from "react"

const Modal = ({ onCancel, children, image }) => {
    return (
        <div onClick={() => onCancel()} className="modal-wrapper active">
            <div onClick={(e) => e.stopPropagation()} className="modal">
                <img
                    className="logo"
                    src={image || require("../images/logo.png")}
                />
                <button type="button" className="close" onClick={onCancel}>
                    &times;
                </button>
                {children}
            </div>
        </div>
    )
}

export default Modal
