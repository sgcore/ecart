import React, { Component } from "react"

const Icon = ({ name, onClick, count, children }) => {
    return (
        <a href="#" onClick={onClick}>
            <table>
                <tbody className="icon-button">
                    <tr>
                        <td>
                            {children}

                            {count ? (
                                <span className="cart-count">{count}</span>
                            ) : (
                                ""
                            )}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{name}</strong>
                        </td>
                    </tr>
                </tbody>
            </table>
        </a>
    )
}

export default Icon
