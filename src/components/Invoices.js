import React, { Component } from "react"
import Image from "./image"

import moment from "moment"
const Invoices = ({ invoices, onSelect }) => {
    return (
        <div className="invoice-box">
            {(invoices || []).map((product) => (
                <div
                    key={product.id}
                    className={
                        "cart-item unselectable pointer " + product.status
                    }
                    onClick={() => onSelect(product.id)}
                >
                    <Image className="product-image" src={product.image} />
                    <div className="product-info">
                        <span className="product-tag">{product.status}</span>
                        <p className="product-name">{product.name}</p>
                        <p className="product-name">{product.description}</p>
                    </div>
                    <div className="product-total">
                        <p className="quantity">
                            {moment(product.date).format("MMMM Do HH:mm")}
                        </p>
                        <p className="amount">
                            {parseFloat(product.amount).toFixed(2)}
                        </p>
                    </div>
                </div>
            ))}
        </div>
    )
}

export default Invoices
