import React, { Component } from "react"

const Footer = ({ name, description }) => {
    return (
        <footer>
            <p>
                &copy; 2020 <strong>{name || "€api"}</strong> -{" "}
                {description || "comercial"}
            </p>
        </footer>
    )
}

export default Footer
