import React, { Component } from "react"

class QuickView extends Component {
    render() {
        return (
            <div className="quick-view">
                <div className="quick-view-image">
                    <img
                        src={this.props.product.image}
                        alt={this.props.product.name}
                        onError={(e) => {
                            e.target.onerror = null
                            e.target.src = require("../images/noImageProduct.png")
                        }}
                    />
                </div>
                <div className="quick-view-details">
                    <span className="product-name">
                        {this.props.product.name}
                    </span>

                    {this.props.product.price ? (
                        <span className="product-price">
                            {parseFloat(this.props.product.price).toFixed(2)}
                        </span>
                    ) : null}
                </div>
            </div>
        )
    }
}

export default QuickView
