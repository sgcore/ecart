import React, { Component } from "react"
import Image from "./image"

const CartItem = ({ product, onRemove }) => {
    return (
        <li className="cart-item" key={product.name}>
            <Image className="product-image" src={product.image} />
            <div className="product-info">
                <p className="product-name">{product.name}</p>
                <p className="product-price">
                    {parseFloat(product.price).toFixed(2)}
                </p>
            </div>
            <div className="product-total">
                <p className="quantity">
                    {parseFloat(product.quantity).toFixed(2)}{" "}
                    {product.metric || "u"}
                </p>
                <p className="amount">
                    {(product.quantity * product.price).toFixed(2)}
                </p>
            </div>
            <a
                className="product-remove"
                href="#"
                onClick={() => onRemove(product.id)}
            >
                ×
            </a>
        </li>
    )
}

export default CartItem
