import React, { Component } from "react"
import { getLanguage } from "../languajes"
const NoResults = () => {
    return (
        <div className="products">
            <div className="no-results">
                <img
                    src={require("../images/itemNotFound.png")}
                    alt={getLanguage().NoItemFound}
                />
                <h2>{getLanguage().NoItemFound}</h2>
            </div>
        </div>
    )
}

export default NoResults
