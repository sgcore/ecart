import React, { Component } from "react"
import { getLanguage } from "../languajes"

const EmptyCart = () => {
    return (
        <div className="empty-cart">
            <img
                src={require("../images/emptyCart.png")}
                style={{ width: "250px", height: "175px" }}
                alt={"empty-cart"}
            />
            <h2>{getLanguage().EmptyCartText}</h2>
        </div>
    )
}

export default EmptyCart
