import React, { Component } from "react"
import ReactDOM from "react-dom"
import axios from "axios"
import Header from "./components/Header"
import Products from "./components/Products"
import Footer from "./components/Footer"
import QuickView from "./components/QuickView"
import Signon from "./components/signon"
import Modal from "./components/Modal"
import Invoice from "./components/Invoice"
import parseProducts from "./scripts/parseProducts"
import { getUser, addInvoiceToUser } from "./scripts/user"
import { getFacilities, getFacility, putInvoice, getInvoice } from "./scripts/calls"
import "./scss/style.scss"
import { getLanguage } from "./languajes"
const url = "http://localhost/mcapi/public/"
class App extends Component {
    constructor() {
        super()
        this.state = {
            products: null,
            cart: [],
            totalItems: 0,
            totalAmount: 0,
            term: "",
            category: "",
            cartBounce: false,
            quickViewProduct: {},
            modalActive: false,
            requireUser: false,
            facilities: [],
            loading: false,
            invoice: null,
        }
        this.handleSearch = this.handleSearch.bind(this)
        this.handleMobileSearch = this.handleMobileSearch.bind(this)
        this.handleCategory = this.handleCategory.bind(this)
        this.handleAddToCart = this.handleAddToCart.bind(this)
        this.sumTotalItems = this.sumTotalItems.bind(this)
        this.sumTotalAmount = this.sumTotalAmount.bind(this)
        this.checkProduct = this.checkProduct.bind(this)
        this.handleRemoveProduct = this.handleRemoveProduct.bind(this)
        this.generateInvoice = this.generateInvoice.bind(this)
        this.openModal = this.openModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
    }
    componentWillMount() {
        getFacility(this, 50)
    }
    generateInvoice(cart) {
        const { facility } = this.state
        const user = getUser()
        if (!user) {
            this.setState({ requireUser: true })
            return false
        }
        const { name, email, dni, id } = user
        let invoice = parseProducts(cart)
        invoice.customer = { name, email, dni, id }
        invoice.facility = {
            id: facility.id,
            name: facility.name,
            code: facility.code,
            image: facility.image,
        }
        return putInvoice(this, invoice).then((i) => {
            if (i) {
                addInvoiceToUser(i)
                return true
            } else {
                return Promise.reject(getLanguage().errorPutingOrder)
            }
        })
    }
    selectInvoice(id) {
        this.setState({ requireUser: false }, () => getInvoice(this, id))
    }
    // Search by Keyword
    handleSearch(event) {
        this.setState({ term: event.target.value })
    }
    // Mobile Search Reset
    handleMobileSearch() {
        this.setState({ term: "" })
    }
    // Filter by Category
    handleCategory(event) {
        this.setState({ category: event.target.value })
    }
    // Add to Cart
    handleAddToCart(selectedProducts) {
        //fix quantity
        selectedProducts.quantity = selectedProducts.quantity || 1
        let cartItem = this.state.cart
        let productID = selectedProducts.id
        let productQty = selectedProducts.quantity
        if (this.checkProduct(productID)) {
            let index = cartItem.findIndex((x) => x.id == productID)
            cartItem[index].quantity = Number(cartItem[index].quantity) + Number(productQty)
            this.setState({
                cart: cartItem,
            })
        } else {
            cartItem.push(selectedProducts)
        }
        this.setState({
            cart: cartItem,
            cartBounce: true,
        })
        setTimeout(
            function () {
                this.setState({
                    cartBounce: false,
                    quantity: 1,
                })
            }.bind(this),
            1000
        )
        this.sumTotalItems(this.state.cart)
        this.sumTotalAmount(this.state.cart)
    }
    handleRemoveProduct(id) {
        let cart = this.state.cart
        let index = cart.findIndex((x) => x.id == id)
        cart.splice(index, 1)
        this.setState({
            cart: cart,
        })
        this.sumTotalItems(this.state.cart)
        this.sumTotalAmount(this.state.cart)
    }
    checkProduct(productID) {
        let cart = this.state.cart
        return cart.some(function (item) {
            return item.id === productID
        })
    }
    sumTotalItems() {
        let total = 0
        let cart = this.state.cart
        total = cart.length
        this.setState({
            totalItems: total,
        })
    }
    sumTotalAmount() {
        let total = 0
        let cart = this.state.cart
        for (var i = 0; i < cart.length; i++) {
            total += cart[i].price * parseFloat(cart[i].quantity)
        }
        this.setState({
            totalAmount: total,
        })
    }

    // Open Modal
    openModal(product) {
        this.setState({
            quickViewProduct: product,
            modalActive: true,
        })
    }
    // Close Modal
    closeModal() {
        this.setState({
            modalActive: false,
        })
    }

    render() {
        const { facility, requireUser, error, loading, invoice } = this.state
        const { name, description } = facility || {}
        return (
            <div className="container">
                <Header
                    cartBounce={this.state.cartBounce}
                    total={this.state.totalAmount.toFixed(2)}
                    totalItems={this.state.totalItems}
                    cartItems={this.state.cart}
                    removeProduct={this.handleRemoveProduct}
                    handleSearch={this.handleSearch}
                    handleMobileSearch={this.handleMobileSearch}
                    handleCategory={this.handleCategory}
                    generateInvoice={this.generateInvoice}
                    categoryTerm={this.state.category}
                    facility={this.state.facility}
                    onLogout={() => this.setState({ requireUser: true, user: null })}
                />
                {invoice && !loading && !error && (
                    <Modal onCancel={() => this.setState({ invoice: null, requireUser: true })}>
                        <Invoice invoice={invoice}></Invoice>
                    </Modal>
                )}
                {loading && (
                    <Modal>
                        <h2 style={{ textAlign: "center" }}>{loading}</h2>
                        <div className="lds-hourglass"></div>
                    </Modal>
                )}
                {error && !loading && (
                    <Modal onCancel={() => this.setState({ error: null })}>
                        <h3>{error}</h3>
                    </Modal>
                )}
                {requireUser && !loading && (
                    <Signon
                        onLogin={() => this.setState({ requireUser: false })}
                        onCancel={() => this.setState({ requireUser: false })}
                        onError={(e) => this.setState({ requireUser: false, error: e })}
                        onSelectInvoice={(i) => this.selectInvoice(i)}
                    />
                )}
                }
                <Products
                    productsList={this.state.products || []}
                    searchTerm={this.state.term}
                    addToCart={this.state.products ? this.handleAddToCart : (f) => getFacility(this, f.id)}
                    openModal={this.openModal}
                    facility={this.state.facility}
                    facilities={this.state.facilities}
                />
                <Footer name={name} description={description} />
                <QuickView
                    product={this.state.quickViewProduct}
                    openModal={this.state.modalActive}
                    closeModal={this.closeModal}
                />
            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById("root"))
