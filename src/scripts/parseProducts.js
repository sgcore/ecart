const parseProducts = (kart) => {
    let items = []
    let errors = []
    let subtotal = 0
    let total = 0
    for (let i = 0; i < kart.length; i++) {
        const { quantity, name, code, price, id, iva, metric } = kart[i] || {}
        const item = {
            quantity: Number(quantity),
            name,
            code,
            id,
            metric: metric || "u",
            iva: iva ? parseFloat(iva) : 21,
            price: price ? parseFloat(price) : 0,
        }
        if (item.code && item.quantity > 0 && item.price > 0) {
            const tot = item.quantity * item.price
            const exent = tot / (1 + item.iva / 100)
            let itemToPush = { total: tot, exent, tax: tot - exent, ...item }
            items.push(itemToPush)
            total += tot
            subtotal += exent
        } else {
            errors.push("Some items did not meet requirements")
        }
    }

    return {
        items,
        subtotal,
        total,
        errors,
    }
}
export default parseProducts
