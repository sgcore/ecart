import axios from "axios"
import { getUser, setUser } from "./user"
import { getFacilityId } from "./facility"
const url = "http://localhost/eapi/public/"
import { getLanguage, setLanguage } from "../languajes"
const response = (p) =>
    Promise.resolve(p)
        .then((response) => {
            if (response.error) {
                throw Error(response.error.data)
            }
            return Promise.resolve(response)
        })
        .catch((e) => {
            const code = e.response.data.code
            if (code == 401) {
                setUser(null)
                throw Error(getLanguage().NotAuthorized)
            } else if (code == 409) throw Error(getLanguage().InconsistentData)
            else throw e
        })
const post = (action, data) => {
    const user = getUser()
    return response(
        axios.post(url + action, data, {
            headers: user && user.token ? { Authorization: `Bearer ${user.token}` } : null,
        })
    )
}
const get = (action) => {
    const user = getUser()
    return response(
        axios.get(url + action, {
            headers: user && user.token ? { Authorization: `Bearer ${user.token}` } : null,
        })
    )
}
const loading = (component, promise, updateStatus = false, loading = "loading...") => {
    return Promise.resolve(component.setState({ loading, error: "" }))
        .then(() => promise)
        .then((result) => {
            let state = updateStatus ? { ...result } : null
            return Promise.resolve(component.setState({ loading: false, ...state })).then(() => Promise.resolve(result))
        })
        .catch((e) => {
            component.setState({ error: e.message, loading: false })
        })
}
const profile = (fid) => {
    return get("api/profile/" + fid).then((response) => {
        setUser({ ...getUser(), ...response.data })
    })
}
const login = (component, user, password) => {
    return loading(
        component,
        post("api/login_check", {
            _username: user,
            _password: password,
        }).then((response) => {
            let newuser = { token: response.data.token }
            setUser(newuser)
            return profile(getFacilityId())
        }),
        false,
        getLanguage().LoadingLogin
    )
}
const signup = (component, email, password, name, dni) => {
    return (
        loading(
            component,
            post("api/register", {
                _name: name,
                _email: email,
                _username: email,
                _password: password,
                _dni: dni,
            })
        ).then((response) => {
            return Promise.resolve(response)
        }),
        false,
        getLanguage().LoadingRegister
    )
}
const getFacility = (component, id) => {
    return loading(
        component,
        get("api/facility/" + id).then((response) => {
            const { content, id, parent, name, code } = response.data
            const mycontent = content && content.includes("{") ? eval("(" + content + ")") : null
            const { products, config } = mycontent
            if (config) setLanguage(config.language)
            return Promise.resolve({
                facility: { id, parent, name, code, ...config },
                products,
            })
        }),
        true,
        getLanguage().LoadingFacility
    )
}
const getFacilities = (component) => {
    return loading(
        component,
        get("api/facilities").then((response) => {
            return Promise.resolve({ facilities: response.data })
        }),
        true,
        getLanguage().LoadingFacilities
    )
}
const putInvoice = (component, invoice) => {
    var query = `mutation { putEntity(input: {
        description:"waiting for approval",
        name: "Invoice created by ${invoice.customer.name}"
        type: "invoice",
        status: "created",
        image: "${invoice.facility.image}"
        amount: ${invoice.total},
        parent:${invoice.facility.id},
        content: "${JSON.stringify(invoice)
            .replace(/"([^"]+)":/g, "$1:")
            .replace(/"/g, '\\"')}"}  ) { id, code, createdat, name, description, image, status, amount }}`
    return loading(
        component,
        post("api/gq", { query }).then((response) => {
            const { data, errors } = response.data
            if (errors) {
                throw Error(errors[0].message)
            }
            const { id, code, createdat, name, description, image, status, amount } = data.putEntity
            return Promise.resolve({
                ...invoice,
                id,
                code,
                date: JSON.parse(createdat).date,
                name,
                description,
                image,
                status,
                amount,
            })
        }),
        true,
        getLanguage().LoadingInvoice
    )
}
const getInvoice = (component, iid) => {
    var query = `query {
        entityById(id: ${iid}) {id, name, code, description, createdat, content}
      }`
    return loading(
        component,
        post("api/gq", { query }).then((response) => {
            const { data, errors } = response.data
            if (errors) {
                throw Error(errors[0].message)
            }
            if (!data.entityById) throw Error("Invoice not found")
            const { id, name, code, description, createdat, content } = data.entityById
            return Promise.resolve({
                invoice: {
                    id,
                    code,
                    name,
                    description,
                    date: JSON.parse(createdat)["date"],
                    ...eval("(" + JSON.parse(content) + ")"),
                },
            })
        }),
        true,
        getLanguage().LoadingInvoice
    )
}

export { login, signup, getFacility, getFacilities, putInvoice, getInvoice }
