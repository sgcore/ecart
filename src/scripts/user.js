let currentUser = null
const getUser = () => {
    if (!currentUser) {
        let stored = localStorage.getItem("user")
        if (stored) {
            try {
                currentUser = JSON.parse(stored)
            } catch (e) {}
        }
    }
    return currentUser
}
const setUser = (user) => {
    currentUser = user
    localStorage.setItem("user", JSON.stringify(user))
}

const addInvoiceToUser = (invoice) => {
    if (getUser() && invoice) {
        let invoices = currentUser.invoices || []
        currentUser.invoices = [invoice, ...invoices]
        localStorage.setItem("user", JSON.stringify(currentUser))
    }
}

export { getUser, setUser, addInvoiceToUser }
